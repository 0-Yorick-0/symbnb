$('#add-image').click(function(){
    //récupération du numéro des futurs champs qui seront crées et conversion en int à la volée à l'aide du '+'
    const counter = +$('#widgets-counter').val();
    //récupération du prototype des entrées (le code html généré par l'option 'allow_add dans la classe ImageType)
    // et remplacement de son index par celui récupéré plus haut
    const template = $('#annonce_images').data('prototype').replace(/__name__/g, counter);

    //injection du template dans la div du sous-formulaire
    $('#annonce_images').append(template);

    $('#widgets-counter').val(counter + 1);

    //gestion du bouton supprimer
    handleDeleteButtons();
});

function handleDeleteButtons() {
    $('button[data-action="delete"]').click(function () {
        //dataset représente tous les attributs data-something
        const target = this.dataset.target;

        $(target).remove();
    })
}
//mise à jour de la valeur du compteur dans le champ caché
function updateCounter() {
    const count = +$('#annonce_images div.form-group').length;

    $('#widgets-counter').val(count);
}
//au chargement de la page
updateCounter();
handleDeleteButtons();