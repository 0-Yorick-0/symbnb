<?php

namespace App\Controller;

use App\Entity\PasswordUpdate;
use App\Entity\User;
use App\Form\AccountType;
use App\Form\PasswordUpdateType;
use App\Form\RegistrationType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AccountController extends AbstractController
{
    /**
     * @Route("/login", name="account_login")
     *
     * @param AuthenticationUtils $utils
     * @return Response
     */
    public function login(AuthenticationUtils $utils)
    {
        $error = $utils->getLastAuthenticationError();
        $lastUsername = $utils->getLastUsername();

        return $this->render('account/login.html.twig', [
            'hasError'     => $error !== null,
            'lastUsername' => $lastUsername
        ]);
    }

    /**
     * @Route(
     *     "/logout",
     *     name="account_logout"
     * )
     *
     * @return void
     */
    public function logout()
    {
        //...rien, Symfony gère tout tout seul
    }

    /**
     * @Route(
     *     "/register",
     *     name="account_register"
     * )
     *
     * @return Response
     */
    public function register(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();

        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $user->setHash(
                $encoder->encodePassword($user, $user->getHash())
            );
            $em->persist($user);
            $em->flush();

            $this->addFlash('success',
                'Votre compte a bien été crée'
            );

            return $this->redirectToRoute('account_login');
        }

        return $this->render('account/registration.html.twig', [
           'form' => $form->createView()
        ]);
    }

    /**
     * @Route(
     *     "/account/profile",
     *     name="account_profile"
     * )
     * @IsGranted("ROLE_USER")
     *
     * @return Response
     */
    public function profile(Request $request, EntityManagerInterface $em)
    {
        //permet de récupérer l'utilisateur connecté
        $user = $this->getUser();
        $form = $this->createForm(AccountType::class, $user);

        if ($form->isSubmitted() && $form->isValid()){
            $em->persist($user);
            $em->flush();

            $this->addFlash('success',
                'Votre compte a bien été modifié'
            );

            return $this->redirectToRoute('homepage');
        }

        return $this->render('account/profile.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route(
     *     "/account/password-update",
     *     name="account_password"
     * )
     * @IsGranted("ROLE_USER")
     *
     * @return Response
     */
    public function updatePassword(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        $passwordUpdate = new PasswordUpdate();

        $form = $this->createForm(PasswordUpdateType::class, $passwordUpdate);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            //Vérifier que le oldPassword est le même que celui rentré par l'utilisateur
            if (!password_verify($passwordUpdate->getOldPassword(), $user->getHash())) {
                //Gérer l'erreur en ajoutant artificiellement une erreur dans le form
                $form->get('oldPassword')->addError(
                    new FormError("Le mot de passe que vous avez tapé n'est pas votre mot de passe actuel")
                );
            }else {
                $hash = $encoder->encodePassword($user, $passwordUpdate->getNewPassword());
                $user->setHash($hash);
                $em->persist($user);
                $em->flush();

                $this->addFlash(
                    'success',
                    "Votre mot de passe a bien été modifié"
                );
                return $this->redirectToRoute('homepage');
            }
        }
        //ici on rend le form, avec ou sans erreur artificelle
        return $this->render('account/password.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Affiche le profil de l'utilisateur connecté
     *
     * @Route(
     *     "/account",
     *     name="account_index"
     * )
     * @IsGranted("ROLE_USER")
     *
     * @return Response
     */
    public function myAccount()
    {
        return $this->render('user/login.html.twig', [
            'user' => $this->getUser()
        ]);
    }

    /**
     * Affiche la liste des réservations faites par l'utilisateur
     *
     * @Route(
     *     "/account/bookings",
     *     name="account_bookings"
     * )
     *
     * @return Response
     */
    public function bookings()
    {
        return $this->render('account/bookings.html.twig');
    }
}
