<?php

namespace App\Controller;

use App\Entity\Ad;
use App\Entity\Image;
use App\Form\AnnonceType;
use App\Repository\AdRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdController extends AbstractController
{
    /**
     * @Route("/ads", name="ads_index")
     */
    public function index(AdRepository $repository)
    {
        return $this->render('ad/login.html.twig', [
            'ads' => $repository->findAll(),
        ]);
    }

    /**
     * Création d'une annonce
     *
     * @Route(
     *     "ads/new",
     *     name="ads_create"
     * )
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function create(Request $request, EntityManagerInterface $em)
    {
        $ad = new Ad();

        $form = $this->createForm(AnnonceType::class, $ad);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($ad->getImages() as $image){
                $em->persist($image);
            }

            $ad->setAuthor($this->getUser());

            $em->persist($ad);
            $em->flush();

            $this->addFlash(
                'success',//type du flash, qui est choisi arbitrairement (mais qui correspondra aux classes bootstrap)
                "L'annonce <strong>{$ad->getTitle()}</strong> a bien été enregistrée"
            );

            return $this->redirectToRoute('ads_show', [
               'slug' => $ad->getSlug()
            ]);
        }

        return $this->render('ad/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Edition d'une annonce
     *
     * @Route(
     *     "/ads/{slug}/edit",
     *     name="ads_edit"
     * )
     * @Security(
     *     "is_granted('ROLE_USER') and user === ad.getAuthor()",
     *     message="Cette annonce ne vous appartient pas, vous ne pouvez pas la modifier"
     * )
     *
     * @see https://symfony.com/doc/current/security/expressions.html
     * @see https://symfony.com/doc/master/bundles/SensioFrameworkExtraBundle/annotations/security.html
     *
     * @param Ad $ad
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function edit(Ad $ad, Request $request, EntityManagerInterface $em)
    {
        $form = $this->createForm(AnnonceType::class, $ad);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($ad->getImages() as $image){
                $em->persist($image);
            }

            $em->persist($ad);
            $em->flush();

            $this->addFlash(
                'success',//type du flash, qui est choisi arbitrairement (mais qui correspondra aux classes bootstrap)
                "L'annonce <strong>{$ad->getTitle()}</strong> a bien été modifiée"
            );

            return $this->redirectToRoute('ads_show', [
                'slug' => $ad->getSlug()
            ]);
        }

        return $this->render('ad/edit.html.twig', [
            'form' => $form->createView(),
            'ad'   => $ad
        ]);
    }

    /**
     * Permet d'afficher une seule annonce
     *
     * @Route(
     *     "/ads/{slug}",
     *     name="ads_show"
     * )
     *
     * @param string $slug
     * @return Response
     */
    public function show(Ad $ad)
    {
        return $this->render('ad/show.html.twig', [
           'ad' => $ad
        ]);
    }

    /**
     * Permet de supprimer une annonce
     *
     * @Route(
     *     "/ads/{slug}/delete",
     *     name="ads_delete"
     * )
     * @Security(
     *     "is_granted('ROLE_USER') and user === ad.getAuthor()",
     *     message="Vous n'avez pas le droit d'accéder à cette ressource"
     * )
     *
     * @param Ad $ad
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete(Ad $ad, EntityManagerInterface $em)
    {
        $em->remove($ad);
        $em->flush();

        $this->addFlash(
            'success',
            "L'annonce a bien été supprimée"
        );

        return $this->redirectToRoute('ads_index');
    }
}
