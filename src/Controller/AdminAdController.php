<?php

namespace App\Controller;

use App\Entity\Ad;
use App\Form\AnnonceType;
use App\Repository\AdRepository;
use App\Service\Pagination;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminAdController extends AbstractController
{
    /**
     * @Route(
     *     "/admin/ads/{page}",
     *     name="admin_ads_index",
     *     requirements={
     *          "page": "\d+"
     *     }
     * )
     */
    public function index(Pagination $pagination, $page = 1)
    {
        $pagination->setEntityClass(Ad::class)
            ->setCurrentPage($page);
        
        return $this->render('admin/ad/index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * Affichage du form d'édition
     *
     * @Route(
     *     "admin/ads/{id}/edit",
     *     name="admin_ads_edit"
     * )
     *
     * @param Ad $ad
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Ad $ad, Request $request, EntityManagerInterface $em)
    {
        $form = $this->createForm(AnnonceType::class, $ad);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($ad);
            $em->flush();

            $this->addFlash(
                'success',
                "L'annonce <strong>{$ad->getTitle()}</strong> a bien été enregistrée"
            );
        }

        return $this->render('admin/ad/edit.html.twig', [
            'ad'   => $ad,
            'form' => $form->createView()
        ]);
    }

    /**
     * Permet de supprimer une annonce
     *
     * @Route(
     *     "/admin/ads/{id}/delete",
     *     name="admin_ads_delete"
     * )
     *
     * @param Ad $ad
     * @param EntityManagerInterface $em
     */
    public function delete(Ad $ad, EntityManagerInterface $em)
    {
        if (count($ad->getBookings()) > 0) {
            $this->addFlash(
                'warning',
                "Vous ne pouvez pas supprimer l'annonce <strong>{$ad->getTitle()}</strong> car elle possède déjà des réservations"
            );
        }else{
            $em->remove($ad);
            $em->flush();

            $this->addFlash(
                'success',
                "L'annonce <strong>{$ad->getTitle()}</strong> a bien été supprimée"
            );
        }

        return $this->redirectToRoute('admin_ads_index');
    }
}
