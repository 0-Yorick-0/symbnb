<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\AdminCommentType;
use App\Repository\CommentRepository;
use App\Service\Pagination;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminCommentController extends AbstractController
{
    /**
     * @Route(
     *     "/admin/comments/{page}",
     *     name="admin_comments_index",
     *     requirements={
     *          "page": "\d+"
     *     }
     * )
     */
    public function index(Pagination $pagination, $page = 1)
    {
        $pagination->setEntityClass(Comment::class)
            ->setCurrentPage($page);
        
        return $this->render('admin/comment/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * Permet de modifier un commentaire
     *
     * @Route(
     *     "/admin/comments/{id}/edit",
     *     name="admin_comments_edit"
     * )
     *
     * @param Comment $comment
     * @param Request $request
     * @param EntityManagerInterface $em
     */
    public function edit(Comment $comment, Request $request, EntityManagerInterface $em)
    {
        $form = $this->createForm(AdminCommentType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($comment);
            $em->flush();

            $this->addFlash(
                'success',
                "Le commentaire numéro {$comment->getId()} a bien été modifié"
            );
        }

        return $this->render('admin/comment/edit.html.twig', [
            'comment' => $comment,
            'form'    => $form->createView()
        ]);
    }

    /**
     * Permet de supprimer un commentaire
     *
     * @Route(
     *     "/admin/comments/{id}/delete",
     *     name="admin_comments_delete"
     * )
     *
     * @param Comment $comment
     * @param EntityManagerInterface $em
     */
    public function delete(Comment $comment, EntityManagerInterface $em)
    {
        $em->remove($comment);
        $em->flush();

        $this->addFlash(
            'success',
            "Le commentaire numéro {$comment->getAuthor()->getFullName()} a bien été supprimé"
        );

        return $this->redirectToRoute('admin_comments_index');
    }
}
