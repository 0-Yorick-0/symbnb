<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Form\AdminBookingType;
use App\Repository\BookingRepository;
use App\Service\Pagination;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminBookingController extends AbstractController
{
    /**
     * @Route(
     *     "/admin/bookings/{page}",
     *     name="admin_bookings_index",
     *     requirements={
     *          "page": "\d+"
     *     }
     *)
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Pagination $pagination, $page = 1)
    {
        $pagination->setEntityClass(Booking::class)
            ->setCurrentPage($page);
        
        return $this->render('admin/booking/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * Permet d'éditer une réservation
     *
     * @Route(
     *     "/admin/bookings/{id}/edit",
     *      name="admin_booking_edit"
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Booking $booking, Request $request, EntityManagerInterface $em)
    {
        $form = $this->createForm(AdminBookingType::class, $booking);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //ceci va provoquer un recalcul auto, car les event PrePersist et PreUpdate
            //font un test sur if(empty($this->amount))
            $booking->setAmount(0);
            $em->persist($booking);
            $em->flush();

            $this->addFlash(
                'success',
                "La réservation n°{$booking->getId()} a bien été modifiée"
            );

            return $this->redirectToRoute("admin_bookings_index");
        }

        return $this->render('admin/booking/edit.html.twig', [
            'form'    => $form->createView(),
            'booking' => $booking
        ]);
    }

    /**
     * Suppression d'une réservation
     *
     * @Route(
     *     "/admin/bookings/{id}/delete",
     *     name="admin_booking_delete"
     * )
     *
     * @param Booking $booking
     * @param EntityManagerInterface $em
     */
    public function delete(Booking $booking, EntityManagerInterface $em)
    {
        $em->remove($booking);
        $em->flush();

        $this->addFlash(
            'success',
            "La réservation a bien été supprimée"
        );

        return $this->redirectToRoute("admin_bookings_index");
    }
}
