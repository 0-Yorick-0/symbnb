<?php
/**
 * Roxed by :
 * User: yorickferlin
 * Date: 25/02/2020
 * No shit !
 */

namespace App\Form\DataTransformer;


use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Class FrenchToDateTimeTransformer
 * @package App\Form\DataTransformer
 * @see https://symfony.com/doc/4.4/form/data_transformers.html
 */
class FrenchToDateTimeTransformer implements DataTransformerInterface
{

    /**
     * @inheritDoc
     *
     * Transforme une date en date au format français
     *
     * @return string
     */
    public function transform($date)
    {
        if($date === null) {
            return '';
        }
        return $date->format('d/m/Y');
    }

    /**
     * @inheritDoc
     *
     * Transforme une date au format français en date
     *
     * @return \DateTime
     */
    public function reverseTransform($frenchDate)
    {
        if ($frenchDate === null) {
            throw new TransformationFailedException("Vous devez fournir une date");
        }

        $date = \DateTime::createFromFormat('d/m/Y', $frenchDate);

        if ($date === false) {
            throw new TransformationFailedException("Le format de la date n'est pas le bon");
        }

        return $date;
    }
}