<?php

namespace App\Form;

use App\Entity\Ad;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AnnonceType, au lieu de AdType, pour éviter d'être bloqué par AdBlock à l'affichage
 * @package App\Form
 */
class AnnonceType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',
                TextType::class,
                $this->getConfiguration("Titre", "Tapez un titre pour votre annonce")
            )
            ->add('slug',
                TextType::class,
                $this->getConfiguration(
                    "Adresse Web", "Tapez l'adresse web (automatique)", [
                        'required' => false
                    ]
                )
            )
            ->add('coverImage',
                UrlType::class,
                $this->getConfiguration("URL de l'image principale", "Donnez l'adresse d'une image")
            )
            ->add('introduction',
                TextType::class,
                $this->getConfiguration("Introduction", "Donnez une description globale de l'annonce")
            )
            ->add('content',
                TextareaType::class,
                $this->getConfiguration("Description détaillée", "Tapez une description")
            )
            ->add('rooms',
                IntegerType::class,
                $this->getConfiguration("Nombre de chambres", "Le nombre de chambres disponibles")
            )
            ->add('price',
                MoneyType::class,
                $this->getConfiguration("Prix par nuit", "Indiquez le prix pour une nuit")
            )
            //ajout d'un sous-formulaire, qui va permettre l'ajout de sous-entité
            ->add('images', CollectionType::class, [
                'entry_type'   => ImageType::class,//on lie ce sous-formulaire a l'entité Image
                'allow_add'    => true, //autorise l'ajout de nouveaux éléments, en insérant du code HTML nécéssaire à l'ajout de nouveaux éléments
                'allow_delete' => true,
                'by_reference' => false, //permet de forcer l'appel à la méthode 'addImages' de l'entité Ad lors de la validation du formulaire
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ad::class,
        ]);
    }
}
