<?php


namespace App\Form;


use Symfony\Component\Form\AbstractType;

class ApplicationType extends AbstractType
{
    /**
     * Permet d'avoir la config de base d'un champ
     *
     * @param $label
     * @param $placeholer
     * @param array $options
     * @return array
     */
    protected function getConfiguration($label, $placeholer, $options = []) {
        //array_merge_recursive évite d'écraser les clefs des tableaux
        return array_merge_recursive([
            'label' => $label,
            'attr' => [
                'placeholder' => $placeholer
            ]
        ], $options);
    }
}