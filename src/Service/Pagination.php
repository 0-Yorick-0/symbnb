<?php
/**
 * Roxed by :
 * User: yorickferlin
 * Date: 27/02/2020
 * No shit !
 */

namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Environment;

/**
 * Classe de pagination qui extrait toute notion de calcul et de récupération de données de nos controllers
 *
 * Elle nécessite après instanciation qu'on lui passe l'entité sur laquelle on souhaite travailler
 */
class Pagination
{
    private $entityClass;
    
    private $limit = 10;
    
    private $currentPage = 1;
    
    private $route;
    
    private $templatePath;
    
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var Environment
     */
    private $twig;
    
    /**
     * Pagination constructor.
     *
     * Pour récupérer la requête au sein du conteneur de service on ne peut pas injecter directement l'objet Request
     * @see https://symfony.com/doc/current/service_container/request.html
     *
     * Le param $templatePath est configuré par défaut dans config/services.yaml
     *
     * @param EntityManagerInterface $em
     * @param Environment            $twig
     * @param RequestStack           $request
     * @param string                 $templatePath
     */
    public function __construct(
        EntityManagerInterface $em,
        Environment $twig,
        RequestStack $request,
        $templatePath
    )
    {
        $this->entityManager = $em;
        $this->twig          = $twig;
        $this->route         = $request->getCurrentRequest()->attributes->get('_route');
        $this->templatePath  = $templatePath;
    }
    
    /**
     * Permet d'afficher le rendu de la navigation au sein d'un template twig !
     *
     * On se sert ici de notre moteur de rendu afin de compiler le template qui se trouve au chemin
     * de notre propriété $templatePath, en lui passant les variables :
     * - page  => La page actuelle sur laquelle on se trouve
     * - pages => le nombre total de pages qui existent
     * - route => le nom de la route à utiliser pour les liens de navigation
     *
     * Attention : cette fonction ne retourne rien, elle affiche directement le rendu
     *
     * @return void
     */
    public function display()
    {
        $this->twig->display($this->templatePath, [
            'page'  => $this->currentPage,
            'pages' => $this->getPages(),
            'route' => $this->route
        ]);
    }
    
    /**
     * Permet de récupérer les données paginées pour une entité spécifique
     *
     * Elle se sert de Doctrine afin de récupérer le repository pour l'entité spécifiée
     * puis grâce au repository et à sa fonction findBy() on récupère les données dans une
     * certaine limite et en partant d'un offset
     *
     * @throws Exception si la propriété $entityClass n'est pas définie
     *
     * @return array
     */
    public function getData()
    {
        if (empty($this->entityClass)) {
            throw new \Exception("Vous n'avez pas spécifié l'entité sur laquelle nouve devons paginer. Utilisez la méthode `setEntityClass()` de votre service `Pagination`");
        }
        //calcul de l'offset
        //ex: si je suis sur la page 1 alors :
        // $start = 1 * 10 - 10 = 0
        //si je suis sur la page 2 alors :
        //$start = 2 * 10 - 10 = 10 etc..
        $offset = $this->currentPage * $this->limit - $this->limit;
        
        $repository = $this->entityManager->getRepository($this->entityClass);
        return $repository->findBy([], [], $this->limit, $offset);
    }
    
    /**
     * Permet de récupérer le nombre de pages qui existent sur une entité particulière
     *
     * Elle se sert de Doctrine pour récupérer le repository qui correspond à l'entité que l'on souhaite
     * paginer (voir la propriété $entityClass) puis elle trouve le nombre total d'enregistrements grâce
     * à la fonction findAll() du repository
     *
     * @throws Exception si la propriété $entityClass n'est pas configurée
     *
     * @return int
     */
    public function getPages()
    {
        if (empty($this->entityClass)) {
            throw new \Exception("Vous n'avez pas spécifié l'entité sur laquelle nouve devons paginer. Utilisez la méthode `setEntityClass()` de votre service `Pagination`");
        }
        
        $repository = $this->entityManager->getRepository($this->entityClass);
    
        $total = count($repository->findAll());
    
        return ceil($total/$this->limit);//arrondi au-dessus
    }
    
    /**
     * @return mixed
     */
    public function getEntityClass()
    {
        return $this->entityClass;
    }
    
    /**
     * @param mixed $entityClass
     */
    public function setEntityClass($entityClass): Pagination
    {
        $this->entityClass = $entityClass;
        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getLimit()
    {
        return $this->limit;
    }
    
    /**
     * @param mixed $limit
     */
    public function setLimit($limit): Pagination
    {
        $this->limit = $limit;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }
    
    /**
     * @param int $currentPage
     */
    public function setCurrentPage(int $currentPage): Pagination
    {
        $this->currentPage = $currentPage;
        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getRoute()
    {
        return $this->route;
    }
    
    /**
     * @param mixed $route
     */
    public function setRoute($route): Pagination
    {
        $this->route = $route;
        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getTemplatePath()
    {
        return $this->templatePath;
    }
    
    /**
     * @param mixed $templatePath
     */
    public function setTemplatePath($templatePath): Pagination
    {
        $this->templatePath = $templatePath;
        return $this;
    }
}